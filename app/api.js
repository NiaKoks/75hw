const express = require('express');
const router = express.Router();
const Vigenere = require('caesar-salad').Vigenere;

router.post('/encode',(req,res)=>{
    const encodeMes = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encoded: encodeMes})
});
router.post('/decode',(req,res)=>{
    const decodeMes = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send(decodeMes)
});

module.exports = router;

