const express = require('express');
const api = require("./app/api");
const app = express();
const cors = require("cors");

app.use(cors());
app.use(express.json());

const port = 8000;

app.use('/api',api);

app.listen(port,()=>{
    console.log(`Server started on ${port} port`);
})